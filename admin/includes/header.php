<div class="col-md-10">
        <div class="container-fluid">
            <div class="row justify-content-end">
                <ul class="nav">
                        <a class="nav-link pt-3" href="#">
                            <form class="form-inline" style="border: 1px solid black;">
                                <input type="text" class="form-control border-0" name="x" placeholder="find your resource">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default bg-light" type="button"><i class="fa fa-search"></i></button>
                                    </span>
                            </form>
                        </a>

                    <li class="nav-item">
                        <a class="icon" href="#"><i class="fa fa-envelope-o"></i><span class="badge badge-light align-top">3</span><span class="sr-only">unread messages</span></a>
                    </li>

                    <li class="nav-item">
                        <a class="icon" href="#"><i class="fa fa-file-text"></i><span class="badge badge-dark align-top">8</span></a>
                    </li>

                    <li class="nav-item">
                        <a class="icon" href="#"><i class="fa fa-user-plus"></i><span class="badge badge-dark align-top">8</span></a>
                    </li>

                    <li class="#">
                        <a class="icon" href="#"><img class="rounded-circle" src="https://picsum.photos/42/42/?random"></a>
                    </li>
                    <div class="#">
                        <a class="btn"  data-toggle="dropdown"><h4 class="font-weight-bold">SAM<i class="fa fa-chevron-down"></i></h4></a>
                            <ul class="dropdown-menu" role="menu">
                                <li class="pl-3"><a href="profile.html">Profile</a></li>
                                <li class="pl-3"><a href="video.html">Videos</a></li>
                                <li class="pl-3"><a href="#greather_than">Greather than</a></li>
                                <li class="pl-3"><a href="#less_than">Less than</a></li>
                                <li class="pl-3"><a href="login.php">Log Out</a></li>
                            </ul>
                    </div>
                </ul>
            </div>
        </div>
<!-- #############________Right-Side_Strat__________############# -->